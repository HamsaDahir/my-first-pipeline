from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/what_is_ramadan')
def what_is_ramadan():
    return render_template('what_is_ramadan.html')

@app.route('/benefits_of_ramadan')
def benefits_of_ramadan():
    return render_template('benefits_of_ramadan.html')

@app.route('/who_observes_ramadan')
def who_observes_ramadan():
    return render_template('who_observes_ramadan.html')

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port=int("3000"))
